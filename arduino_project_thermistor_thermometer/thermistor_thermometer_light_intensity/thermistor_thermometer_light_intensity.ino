int ThermistorPin = 15;
int LightResistorPin = 16;
int LED_BLUE = 19;
int LED_RED = 18;
int LED_YELLOW = 17;


int Vo;
int Vlight;
float R1 = 10000;
float logR2, R2, T;
float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;

void blink(int, int);

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(LED_YELLOW, OUTPUT);
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_BLUE, OUTPUT);
Serial.begin(9600);
}

void loop() {

  Vo = analogRead(ThermistorPin);
  Vlight = analogRead(LightResistorPin);
  R2 = R1 * (1023.0 / (float)Vo - 1.0);
  logR2 = log(R2);
  T = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2));
  T = T - 273.15;
  /* T = (T * 9.0)/ 5.0 + 32.0; */

  blink(LED_BUILTIN, 50);
  blink(LED_BUILTIN, 50);

  Serial.print("T:"); 
  Serial.print(T);
  Serial.print(", L:"); 
  Serial.println(Vlight);
  //Serial.println(" C"); 

  //delay(500);
  blink(LED_YELLOW, 500);
  blink(LED_RED, 200);
  blink(LED_BLUE, 700);
  blink(LED_YELLOW, 200);
  blink(LED_BLUE, 700);
}


void blink(int color, int del=500)
{
  digitalWrite(color, HIGH);  // turn the LED on (HIGH is the voltage level)
  delay(del);                      // wait for a second
  digitalWrite(color, LOW);   // turn the LED off by making the voltage LOW
  delay(del);                      // wait for a second
}