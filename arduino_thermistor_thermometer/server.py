# Generated by sila2.code_generator; sila2.__version__: 0.10.5

from typing import Optional
from uuid import UUID, uuid4
import logging

from sila2.server import SilaServer

from .feature_implementations.deviceinfoprovider_impl import DeviceInfoProviderImpl
from .feature_implementations.lightintensitycontroller_impl import LightIntensityControllerImpl
from .feature_implementations.loggingservice_impl import LoggingServiceImpl
from .feature_implementations.simulationcontroller_impl import SimulationControllerImpl
from .feature_implementations.temperaturecontroller_impl import TemperatureControllerImpl
from .generated.deviceinfoprovider import DeviceInfoProviderFeature
from .generated.lightintensitycontroller import LightIntensityControllerFeature
from .generated.loggingservice import LoggingServiceFeature
from .generated.simulationcontroller import SimulationControllerFeature
from .generated.temperaturecontroller import TemperatureControllerFeature

from arduino_thermistor_thermometer.serial_interface import (
    SerialInterface,
    SerialInterfaceSimulation,
)

logger = logging.getLogger(__name__)

class Server(SilaServer):
    def __init__(
        self,
        server_uuid: Optional[UUID] = None,
        name: Optional[str] = None,
        description: Optional[str] = None,
        serial_port: Optional[str] = None,
        simulation: bool = False,
    ):
        # TODO: fill in your server information
        if name is None:
            name = "TODO"
        if description is None:
            description = "TODO"
        super().__init__(
            server_name=name,
            server_description=description,
            server_type="TODO",
            server_version="0.1",
            server_vendor_url="https://gitlab.com/SiLA2/sila_python",
            server_uuid=server_uuid if server_uuid is not None else uuid4(),
        )

        self.logger = logger

        if simulation:
            self.hardware_interface = SerialInterfaceSimulation()
        else:
            self.hardware_interface = SerialInterface(serial_port=serial_port)

        self.deviceinfoprovider = DeviceInfoProviderImpl(self)
        self.set_feature_implementation(DeviceInfoProviderFeature, self.deviceinfoprovider)

        self.lightintensitycontroller = LightIntensityControllerImpl(self)
        self.set_feature_implementation(LightIntensityControllerFeature, self.lightintensitycontroller)

        self.loggingservice = LoggingServiceImpl(self)
        self.set_feature_implementation(LoggingServiceFeature, self.loggingservice)

        self.simulationcontroller = SimulationControllerImpl(self)
        self.set_feature_implementation(SimulationControllerFeature, self.simulationcontroller)

        self.temperaturecontroller = TemperatureControllerImpl(self)
        self.set_feature_implementation(TemperatureControllerFeature, self.temperaturecontroller)
