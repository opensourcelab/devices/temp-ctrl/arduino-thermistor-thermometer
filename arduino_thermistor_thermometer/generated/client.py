# Generated by sila2.code_generator; sila2.__version__: 0.10.5
from __future__ import annotations

from typing import Set

from sila2.client import SilaClient
from sila2.framework import FullyQualifiedFeatureIdentifier

from . import deviceinfoprovider, lightintensitycontroller, loggingservice, simulationcontroller, temperaturecontroller


class Client(SilaClient):
    DeviceInfoProvider: deviceinfoprovider.DeviceInfoProviderClient

    LightIntensityController: lightintensitycontroller.LightIntensityControllerClient

    LoggingService: loggingservice.LoggingServiceClient

    SimulationController: simulationcontroller.SimulationControllerClient

    TemperatureController: temperaturecontroller.TemperatureControllerClient

    _expected_features: Set[FullyQualifiedFeatureIdentifier] = {
        FullyQualifiedFeatureIdentifier("org.silastandard/core/SiLAService/v1"),
        FullyQualifiedFeatureIdentifier("de.unigreifswald.biochemie/device/DeviceInfoProvider/v1"),
        FullyQualifiedFeatureIdentifier("de.unigreifswald/examples/LightIntensityController/v1"),
        FullyQualifiedFeatureIdentifier("de.unigreifswald/infrastructure/LoggingService/v0"),
        FullyQualifiedFeatureIdentifier("org.silastandard/core/SimulationController/v1"),
        FullyQualifiedFeatureIdentifier("org.silastandard/examples/TemperatureController/v1"),
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._register_defined_execution_error_class(
            deviceinfoprovider.DeviceInfoProviderFeature.defined_execution_errors["OutputFormatNotSupported"],
            deviceinfoprovider.OutputFormatNotSupported,
        )

        self._register_defined_execution_error_class(
            lightintensitycontroller.LightIntensityControllerFeature.defined_execution_errors[
                "LightIntensityNotReachable"
            ],
            lightintensitycontroller.LightIntensityNotReachable,
        )

        self._register_defined_execution_error_class(
            lightintensitycontroller.LightIntensityControllerFeature.defined_execution_errors["ControlInterrupted"],
            lightintensitycontroller.ControlInterrupted,
        )

        self._register_defined_execution_error_class(
            loggingservice.LoggingServiceFeature.defined_execution_errors["LogginStreamNotAvailable"],
            loggingservice.LogginStreamNotAvailable,
        )

        self._register_defined_execution_error_class(
            loggingservice.LoggingServiceFeature.defined_execution_errors["LogFileNotAvailable"],
            loggingservice.LogFileNotAvailable,
        )

        self._register_defined_execution_error_class(
            simulationcontroller.SimulationControllerFeature.defined_execution_errors["StartSimulationModeFailed"],
            simulationcontroller.StartSimulationModeFailed,
        )

        self._register_defined_execution_error_class(
            simulationcontroller.SimulationControllerFeature.defined_execution_errors["StartRealModeFailed"],
            simulationcontroller.StartRealModeFailed,
        )

        self._register_defined_execution_error_class(
            temperaturecontroller.TemperatureControllerFeature.defined_execution_errors["TemperatureNotReachable"],
            temperaturecontroller.TemperatureNotReachable,
        )

        self._register_defined_execution_error_class(
            temperaturecontroller.TemperatureControllerFeature.defined_execution_errors["ControlInterrupted"],
            temperaturecontroller.ControlInterrupted,
        )
